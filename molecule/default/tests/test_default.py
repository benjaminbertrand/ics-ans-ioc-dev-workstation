import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('ioc_dev_workstations')


def test_installed_packages(host):
    for package in ("telnet", "xfce4-session", "nomachine"):
        assert host.package(package).is_installed


def test_csstudio_installed(host):
    assert host.file("/opt/cs-studio").is_symlink


def test_procserv_installed(host):
    cmd = host.run('/usr/bin/procServ --version')
    assert cmd.stdout.startswith('procServ Process Server')


def test_conda_installed(host):
    cmd = host.run('/opt/conda/bin/conda --version')
    assert cmd.rc == 0
    assert cmd.stdout.startswith('conda')


def test_ethercat_kernel_modules_loaded(host):
    cmd = host.run("/usr/sbin/lsmod")
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-ioc-dev-workstation-ethercat":
        assert "ec_master" in cmd.stdout
        assert "ec_generic" in cmd.stdout
    else:
        assert "ec_master" not in cmd.stdout
        assert "ec_generic" not in cmd.stdout


def test_ethercat_service(host):
    service = host.service("ethercat")
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-ioc-dev-workstation-ethercat":
        assert service.is_running
        assert service.is_enabled
    else:
        assert not service.is_running


def test_node_red_installed(host):
    cmd = host.run("node-red --help")
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-ioc-dev-workstation-ethercat":
        assert cmd.rc == 0
        assert "Usage: node-red" in cmd.stdout
    else:
        assert cmd.rc == 127
